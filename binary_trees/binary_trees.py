class Node():
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def insert(self, k):
        if k <= self.data:
            if self.left is None:
                self.left = Node(k)
            else:
                self.left.insert(k)
        else:
            if self.right is None:
                self.right = Node(k)
            else:
                self.right.insert(k)


def inorder(t):
    if t is not None:
        inorder(t.left)
        print(t.data)
        inorder(t.right)


def preorder(t):
    if t is not None:
        print(t.data)
        inorder(t.left)
        inorder(t.right)


def postorder(t):
    if t is not None:
        inorder(t.left)
        inorder(t.right)
        print(t.data)


def tree_from_array(a):
    tree = Node(a[0])
    aa = iter(a)
    next(aa)

    for a in aa:
        tree.insert(a)

    return tree


def show_traversals(tree):
    print('In-order tree traversal:')
    inorder(tree)
    print('\n')

    print('Pre-order tree traversal:')
    preorder(tree)
    print('\n')

    print('Post-order tree traversal:')
    postorder(tree)
    print('\n')


def run_tests():
    a = [4, 2, 6, 1, 3, 5, 7]
    tree = tree_from_array(a)
    inorder(tree)


def main():
    run_tests()


main()
